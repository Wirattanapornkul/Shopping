/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopping;

import java.util.Hashtable;
import java.util.concurrent.ThreadLocalRandom;
import javax.annotation.PostConstruct;
import javax.ejb.Stateful;

/**
 *
 * @author CSTURoom111
 */
@Stateful
public class Cart implements CartLocal {

    private Hashtable<String,Integer> shoppingCart;
    private String customerID;
    
    public String getCustomerID() {
        return customerID;
    }
    
    @PostConstruct
    public void initialize(){
        shoppingCart = new Hashtable<String,Integer>();
        customerID = Integer.toString(ThreadLocalRandom.current().nextInt(0, 100+1));
    }
    
    @Override
    public void putItem(String itemID, int quantity) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(shoppingCart.containsKey(itemID) == false){
            shoppingCart.put(itemID, quantity);
        }else{
            shoppingCart.replace(itemID, shoppingCart.get(itemID).intValue() + quantity);
        }
    }

    @Override
    public void removeItem(String itemId) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    
    }

    @Override
    public Hashtable<String, Integer> getItem() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        return shoppingCart;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")



}
